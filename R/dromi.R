#' Functions for manipulating dromi studies
#'
#' dromi provides tools for reading and graphing the results of dromi
#' studies.
#'
#' @docType package
#' @name dromi-package
#' @aliases dromi
NULL

#' @title The default root address for dromi studies.
#' @export
#'
#' @description
#' As such, it is useful to have it around, means we don't have to
#' constantly type it ourselves. In the future, this may also be
#' used as a default.
def.root <- "http://localhost:8080"

# Endpoints
ep.study <- "/study"
ep.study_dims <- "/study/dims"
ep.study_runs <- "/study/runs"

#' @title Study overview
#' @export
#'
#' @description
#' \code{study.overview} gets an overview of the study.
#' Most of the time you will not need this, unless you want to get
#' at the number of replications there are in the study.
#'
#' @details
#' The number of replications should not be confused with the number of runs
#' per experiment; you may thank the english language. Instead, it is the number
#' of runs per experiment minus one. Thus, it is truly the number of replications.
#'
#' @return a list with 8 elements:
#'  \item{name}{string name of the experiment, usually just an identifier}
#'  \item{description}{string english description of the experiment, usually only one line}
#'  \item{rscript}{boolean whether the study has an R script attached}
#'  \item{experiments}{number of experiments in total (product of dimension sizes)}
#'  \item{replications}{number of replications for each experiment}
#'  \item{runs}{total number of runs in the study}
#'  \item{simduration}{duration of each simulation}
#'  \item{simreset}{reset time for each simulation}
study.overview <- function(root=def.root, totime=godur.hour) {
    study <- read.csv(dromi.ep(root, ep.study), stringsAsFactors=F)
    study$rscript <- study$rscript == "true"

    l <- list(
        name=study$name,
        description=study$description,
        rscript=study$rscript,
        experiments=as.numeric(study$numExperiments),
        replications=as.numeric(study$numReplications),
        runs=as.numeric(study$numRuns),
        parallel=as.numeric(study$numParallel),
        simduration=totime(study$simulationDuration),
        simreset=totime(study$resetTime)
    )
    return (l)
}

#' @title Study dimensions
#' @export
#'
#' @description
#' \code{study.dims} gets the dimension ids, names, and sizes.
#'
#' @details
#' The number of experiments in the study is the product of the sizes
#' of the dimensions. The number of replications can be taken from
#' \link{study.overview}.
#'
#' @return a data frame with 3 fields:
#'  \item{id}{capitalized ID of the dimension}
#'  \item{name}{english name of the dimension}
#'  \item{size}{number of elements in the dimension}
study.dims <- function(root=def.root, reverse=FALSE) {
    # While dims$id /is/ actually a factor, it doesn't help much at
    # the moment, because then it indexes as a numeric instead of a character.
    # If at some point, having it as a factor is important, this can be done
    # easily with:
    #
    #   dims$id <- as.factor(dims$id)
    dims <- read.csv(dromi.ep(root, ep.study_dims), stringsAsFactors=F)
    dims$id <- toupper(dims$id)
    if (reverse) dims <- dims[rev(rownames(dims)),]
    return (dims)
}

#' @title Get all the runs of a study
#' @export
#'
#' @details
#' The most important part of \code{study.runs} is the \code{endpoint}
#' variable. Most other functions use this variable to find data for
#' graphing, etc.
#'
#' On the other hand, variables such as \code{id} or \code{progress}
#' are pretty useless.
#'
#' @return a data frame with 5 + (dimensions)*2 number of variables:
#'  \item{id}{unique number for each run, starting with 0}
#'  \item{exp}{the experiment number of each run, starting with 0}
#'  \item{repl}{the current replication the run represents, starting with 0}
#'  \item{endpoint}{address of the simulation, very important}
#'  \item{progress}{floating point number of progress percentage, from 0 to 1}
#'  \item{<DIM_ID>}{the dimension ID, as found in \link{study.dims}, from 0}
#'  \item{<DIM_ID>.val}{the value that the dimension takes, as a comment string}
study.runs <- function(root=def.root) {
    read.csv(dromi.ep(root, ep.study_runs), stringsAsFactors=F)
}

#' @title Get the values of all the study dimensions
#' @export
#'
#' @return a list with all the dimensions, with the elements named after the
#' dimension IDs, and the values are the values that the respective dimensions
#' take.
study.dimv <- function(dims, runs) {
    # For compatibility, dims$id can be a factor.
    dims.id <- as.character(dims$id)
    lst <- vector(mode="list", length=length(dims.id))
    names(lst) <- dims.id
    for (id in dims.id) {
        lst[[id]] <- unique(runs[[paste(id, ".val", sep="")]])
    }
    return (lst)
}

#' @title Select factor dimension values
#' @export
study.dimv_select <- function(dims, runs) {
    dimv <- study.dimv(dims, runs)
    a <- dims[1,]; b <- dims[2,]
    if (!is.numeric(dimv[[dims$id[1]]])) {
        # reverse the dimensions
        a <- dims[2,]; b <- dims[1,]
    }
    return (dimv[[b$id]])
}

#' @export
study.run_endpoints <- function(root=def.root) {
    lines <- readLines(paste(root, "/run-0.0/endpoints", sep=""))
    lines <- substring(lines, 9)
    lines <- lines[lines != ""]
}

#' @export
#' @import stringr
study.filters <- function(root=def.root) {
    pattern <- "\\.filter_([^./]+)([^/]+)?([/].*)?"
    filterpat <- "\\.filter_([^./]+)"
    eps <- study.run_endpoints(root)
    matches <- eps[str_detect(eps, pattern)]
    if (length(matches) == 0) {
        data.frame(id=c(), base=c(), ep=c(), stringsAsFactors=F)
    } else {
        ids <- str_match(matches, pattern)[,2]
        orig <- str_replace(matches, filterpat, "")
        ok <- is.na(str_match(matches, pattern)[,4])
        data.frame(id=ids[ok], base=orig[ok], ep=matches[ok], stringsAsFactors=F)
    }
}

#' @export
dromi.sample <- function(s, n=1000, unit="ms") {
    xs <- system2("sample", paste("-n", n, "--unit", unit, "'", s, "'"), T)
    as.numeric(xs)
}

# --------------------------------------------------------------------------------------- #

# dromi.ep expects root to be the URL to the dromi root, like: "http://localhost:8080".
dromi.ep <- function(root, ep) paste(root, ep, "?type=csv", sep="")
dromi.rep <- function(root, runs, ep) paste(root, runs, ep, "?type=csv", sep="")

dromi.read <- function(root, runs, ep, asnum=c(), astime=c(), totime=godur.hour) {
    v <- read.csv(dromi.rep(root, runs, ep), colClasses="character")
    for (col in asnum) {
        if (col %in% names(v)) {
            v[col] <- as.numeric(v[[col]])
        }
    }
    for (col in astime) {
        if (col %in% names(v)) {
            v[col] <- totime(v[[col]])
        }
    }
    return (v)
}

dromi.readall <- function(root, runs, ep, asnum=c(), astime=c(), totime=godur.hour) {
    v <- dplyr::bind_rows(lapply(dromi.rep(root, runs, ep), read.csv, colClasses="character"))
    for (col in asnum) {
        if (col %in% names(v)) {
            v[col] <- as.numeric(v[[col]])
        }
    }
    for (col in astime) {
        if (col %in% names(v)) {
            v[col] <- totime(v[[col]])
        }
    }
    return (v)
}
